<?php
/*
Plugin Name: Testimonial Widget
Description: A simple testimonial widget to display client testimonials and feedbacks.
Version: 1.0.0
Author: SuperFastBusiness
Author URI: http://www.superfastbusiness.com
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt
Bitbucket Plugin URI: https://bitbucket.org/webdevsuperfast/testimonial-widget
*/

// Add Widget Class
if ( !class_exists( 'TW_Widget' ) )
	include_once( plugin_dir_path( __FILE__ ) . 'lib/classes/widget-class.php' );

// AQ Resize
if( !class_exists( 'Aq_Resize' ) )
    require_once( plugin_dir_path( __FILE__ ) . 'lib/classes/aq_resizer.php' );

// Widgets
$widgets = glob( plugin_dir_path( __FILE__ ) . 'lib/widgets/*.php', GLOB_NOSORT );
if ( is_array( $widgets ) ) {
	foreach ( $widgets as $widget ) {
		include_once $widget;
	}
}

// Functions
$functions = glob( plugin_dir_path( __FILE__ ) . 'lib/functions/*.php', GLOB_NOSORT );
if ( is_array( $functions ) ) {
	foreach ( $functions as $function ) {
		require_once $function;
	}
}

// Check if Genesis is installed
register_activation_hook( __FILE__, 'tw_up_activation_check' );
function tw_up_activation_check() {
	$theme_info = get_theme_data( TEMPLATEPATH . '/style.css' );

	// need to find a way to check active themes is MultiSites	- This does not work in new 3.1 network panel.
	if( basename( TEMPLATEPATH ) != 'genesis' ) {
		deactivate_plugins( plugin_basename( __FILE__ ) ); // Deactivate ourself
		wp_die( 'Sorry, you can\'t activate unless you have installed <a href="http://www.studiopress.com/themes/genesis">Genesis</a>' );
	}
}

// Scripts
add_action( 'admin_enqueue_scripts', 'tw_widget_enqueue_scripts' );
function tw_widget_enqueue_scripts( $hook ){
	//Get current page
	$current_page = get_current_screen();

	//Only load if we are not on the nav menu page - where some of our scripts seem to be conflicting
	if ( $current_page->base != 'nav-menus' ){

	/* if ( $hook != 'widgets.php' )
		return; */

	// Image Upload
	wp_enqueue_media();
	wp_enqueue_script( 'widget-image-upload', plugin_dir_url( __FILE__ ) . 'assets/js/image-upload.js', array( 'jquery' ) );
	}
}

add_action( 'wp_enqueue_scripts', 'tw_do_widget_scripts' );
function tw_do_widget_scripts() {
	wp_register_style( 'tw-css', plugin_dir_url( __FILE__ ) . 'assets/css/style.css' );
	wp_enqueue_style( 'tw-css' );

	wp_register_script( 'tw-js', plugin_dir_url( __FILE__ ) . 'assets/js/app.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'tw-js' );
}

// Before Testimonial Content
function tw_testimonial_before_content( $instance ) {
	do_action( 'tw_testimonial_before_content', $instance );
}

// After Testimonial Content
function tw_testimonial_content( $instance ) {
	do_action( 'tw_testimonial_content', $instance );
}

// After Testimonial Content
function tw_testimonial_after_content( $instance ) {
	do_action( 'tw_testimonial_after_content', $instance );
}

// After Testimonial Loop
function tw_testimonial_after_loop( $instance ) {
	do_action( 'tw_testimonial_after_loop', $instance );
}

// Custom Image Function
function tw_post_image() {
	global $post;
	$image = '';
	$image_id = get_post_thumbnail_id( $post->ID );
	$image = wp_get_attachment_image_src( $image_id, 'full' );
	$image = $image[0];
	if ( $image ) return $image;
	return tw_get_first_image();
}
